# vim-highlightedyank

## Usage

The highlight is automatically triggered by the `TextYankPost` event.

## Configuration

### Highlight duration

Use `g:highlightedyank_highlight_duration` or `b:highlightedyank_highlight_duration` to configure the highlight duration. Defaults to 1000.

### Highlight colouring

```vim
highlight Yank cterm=reverse gui=reverse
```
