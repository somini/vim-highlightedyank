" FIXME: Highlight region is incorrect when an input ^V[count]l ranges
"        multiple lines.

function! highlightedyank#hl(regtype) abort
  if v:event.operator !=# 'y' || v:event.regtype ==# ''
    return
  endif

  " TODO
  " if a:regtype ==# 'v'
  " elseif a:regtype ==# 'V'
  " elseif a:regtype[0] ==# "\<C-v>"

  let htime = get(b:, 'highlightedyank_highlight_duration', get(g:, 'highlightedyank_highlight_duration', 1000))

  let bnr = bufnr('%')
  if exists('*nvim_create_namespace')
    let ns = nvim_create_namespace('')
  else
    " Generate a new `src_id`, legacy API
    let ns = nvim_buf_add_highlight(bnr, 0, '', 0, 0, 0)
  endif
  call s:stop(bnr, ns)

  let [_, lin1, col1, off1] = getpos("'[")
  let [lin1, col1] = [lin1 - 1, col1 - 1]
  let [_, lin2, col2, off2] = getpos("']")
  let [lin2, col2] = [lin2 - 1, col2]
  for l in range(lin1, lin1 + (lin2 - lin1))
    let is_first = (l == lin1)
    let is_last = (l == lin2)
    let c1 = is_first ? (col1 + off1) : 0
    let c2 = is_last ? (col2 + off2) : -1
    call nvim_buf_add_highlight(bnr, ns, 'Yank', l, c1, c2)
  endfor
  call timer_start(htime, {-> s:stop(bnr, ns) })
endfunc

function! s:stop(bnr, ns) abort
  if exists('*nvim_create_namespace')
    call nvim_buf_clear_namespace(a:bnr, a:ns, 0, -1)
  else
    call nvim_buf_clear_highlight(a:bnr, a:ns, 0, -1)
  endif
endfunction


" vim:set foldmethod=marker commentstring="%s ts=2 sts=2 sw=2
